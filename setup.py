from setuptools import setup, find_packages
import importlib
import re
import site
import sys
import subprocess

# check if the installation is within a venv, and if so, if include-system-site-packages is set to true.
if sys.prefix == sys.base_prefix:
    # not in venv
    sys.path.append(site.USER_SITE)
    sys.path.extend(site.getsitepackages())
else:
    # in venv
    pyFILE=sys.prefix + '/pyvenv.cfg'
    try:
        pystrline = open(pyFILE, "rt").read()
        pyVSRE = r'include-system-site-packages =.(\S+)'
        pymo = re.search(pyVSRE, pystrline)
    except:
        pymo = None
    if pymo is not None:
        if pymo.group(1) == 'true':
            sys.path.append(site.USER_SITE)
    sys.path.extend(site.getsitepackages())

# remove files that may cause installation problems. They are not needed for using the package
def clean_package(directories):
    for directory in directories:
        try:
            subprocess.run(["rm","-rf",directory])
        except:
            pass
clean_package(["build","dist","*.egg-info"])

def get_package_version(package_str):
    try:
        package_path = importlib.util.find_spec(package_str)
        # read the version file from this package
        VERSIONFILE=package_path.origin.rsplit('/',1)[0] + '/_version.py'
        try:
            verstrline = open(VERSIONFILE, "rt").read()
            VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
            mo = re.search(VSRE, verstrline, re.M)
            if mo:
                pkg_version = mo.group(1)
            else:
                pkg_version=None
        except:
            pkg_version=None
    except:
        # if the module doesn't exist:
        pkg_version = None
    return pkg_version
        
def get_package_version_hist(package_url):
    use_url = package_url.split('//')[1]
    use_url = use_url.replace("git@","https://") + ".git"
    tags_output = subprocess.run(["git","ls-remote","--tags",use_url],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    if tags_output.stderr.decode("utf-8") != '':
        print('ERROR: no git repository found at ',use_url,'. Make sure the url is correct and the access permessions are met.')
        sys.exit(1)
    pkg_version_hist = []
    tags_output_list = tags_output.stdout.decode("utf-8").split('\n')
    for line in tags_output_list:
        if line != '':
            tag = line.split('\t')[1].split('/')[2].split('^')[0]
            if tag not in pkg_version_hist and tag[0] == 'v':
                pkg_version_hist.append(tag)
    return pkg_version_hist
    
def require_custom_package(package_str,version_condition,version_req,git_url):
    # turn version requirements into integer
    version_req_int = [int(i) for i in re.findall(r'\d+',version_req)]
    version_req_int.extend([0 for i in range(3-len(version_req_int))])
    version_req_long = int(sum([i*10**(6-j*3) for j,i in enumerate(version_req_int)]))
    
    # check if package currently exists, and which version
    pkg_version = get_package_version(package_str)

    if pkg_version is None:
        # package does not exist.
        add_pkg = True
    else:
        # turn current package version into integer
        pkg_version_int = [int(i) for i in re.findall(r'\d+',pkg_version)]
        pkg_version_int.extend([0 for i in range(3-len(pkg_version_int))])
        pkg_version_long = int(sum([i*10**(6-j*3) for j,i in enumerate(pkg_version_int)]))
    
        add_pkg = False
        
        # apply the version logic
        if version_condition == ">=" and pkg_version_long < version_req_long:
            add_pkg = True
        elif version_condition == ">" and pkg_version_long <= version_req_long:
            add_pkg = True
        elif version_condition == "=" and pkg_version_long != version_req_long:
            add_pkg = True  
        elif version_condition == "<" and pkg_version_long >= version_req_long:
            add_pkg = True
        elif version_condition == "<=" and pkg_version_long > version_req_long:
            add_pkg = True
    
    if add_pkg:
        append_tag = ''
        # need to get the tags from the git repo
        pkg_version_hist = get_package_version_hist(git_url)
        pkg_version_hist_long = []
        for ver_hist in pkg_version_hist:
            ver_int = [int(i) for i in re.findall(r'\d+',ver_hist)]
            ver_int.extend([0 for i in range(3-len(ver_hist))])
            pkg_version_hist_long.append(int(sum([i*10**(6-j*3) for j,i in enumerate(ver_int)])))
        
        if version_condition == ">=" or version_condition == ">":
            iver_to_add = pkg_version_hist_long.index(max(pkg_version_hist_long))
            if version_condition == ">=" and version_req_long > pkg_version_hist_long[iver_to_add]:
                print('ERROR: A tag with version',version_condition,version_req,'does not exist on',git_url,'. The full list of tags is',*pkg_version_hist)
                sys.exit(1)
            elif version_condition == ">" and version_req_long >= pkg_version_hist_long[iver_to_add]:
                print('ERROR: A tag with version',version_condition,version_req,'does not exist on',git_url,'. The full list of tags is',*pkg_version_hist)
                sys.exit(1)
            append_tag += '@' + pkg_version_hist[iver_to_add]
        elif version_condition == '=':
            if version_req_long not in pkg_version_hist_long:
                print('ERROR: A tag with version',version_condition,version_req,'does not exist on',git_url,'. The full list of tags is',*pkg_version_hist)
                sys.exit(1)
            iver_to_add = pkg_version_hist_long.index(version_req_long)
            append_tag += '@' + pkg_version_hist[iver_to_add]
        elif version_condition == "<":
            min_diff = 1e10
            iver_to_add = pkg_version_hist_long.index(min(pkg_version_hist_long))
            if version_req_long <= pkg_version_hist_long[iver_to_add]:
                print('ERROR: A tag with version',version_condition,version_req,'does not exist on',git_url,'. The full list of tags is',*pkg_version_hist)
                sys.exit(1)
            for m,ver in enumerate(pkg_version_hist_long):
                if ver < version_req_long:
                    if version_req_long - ver < min_diff:
                        min_diff = version_req_long-ver
                        iver_to_add = m
            append_tag += '@' + pkg_version_hist[iver_to_add]
        elif version_condition == "<=":
            min_diff = 1e10
            iver_to_add = pkg_version_hist_long.index(min(pkg_version_hist_long))
            if version_req_long < pkg_version_hist_long[iver_to_add]:
                print('ERROR: A tag with version',version_condition,version_req,'does not exist on',git_url,'. The full list of tags is',*pkg_version_hist)
                sys.exit(1)
            for m,ver in enumerate(pkg_version_hist_long):
                if ver <= version_req_long:
                    if version_req_long - ver < min_diff:
                        min_diff = version_req_long-ver
                        iver_to_add = m
            append_tag += '@' + pkg_version_hist[iver_to_add]
        
        git_url = git_url + append_tag
        
    return add_pkg, git_url

# get the version from the version file
VERSIONFILE="ehpairquantization/_version.py"
verstrline = open(VERSIONFILE, "rt").read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, verstrline, re.M)
if mo:
    verstr = mo.group(1)
else:
    raise RuntimeError("Unable to find version string in %s." % (VERSIONFILE,))

# read the contents of your README file
with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()
    
# list of required packages
instl = ['numpy',
         'scipy',
        ]

# determined if physicsvalues package needs to be installed
add_this_pkg, new_git_url = require_custom_package('physicsvalues','>=','2.2.0','dmmodeling_physicsvalues @ git+https://git@gitlab.com/supercdms/public/dmmodeling_physicsvalues')
if add_this_pkg:
    instl.append(new_git_url)

setup(
    name="dmmodeling_ehpairquantization",
    version=verstr,
    long_description=long_description,
    author="Matthew Wilson",
    author_email="matthew.wilson@kit.edu",
    url="https://gitlab.com/supercdms/public/dmmodeling_ehpairquantization",
    packages=find_packages(),
    install_requires=instl,
data_files=[
    'ehpairquantization/RK_ionization/interpolator.pkl',
    ],
    include_package_data=True,
    python_requires=">=3",
)