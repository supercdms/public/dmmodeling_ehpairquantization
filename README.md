# DM Modeling ehPairQuantization Package

Repository and python package for computing electron hole (eh) pair ionization probabilities using various ionization yield models using various models. This package is primarily used for dark matter modeling.

This package is made available by the SuperCDMS Collaboration to provide in the public domain the software used for SuperCDMS studies. Though validated by the Collaboration, there is no warranty or guarantee that it is correct. Users should perform their own validation of the functions contained within this package.

Contacts - Taylor Aralis (taralis@caltech.edu, tbaralis@gmail.com), Matthew Wilson (matthew.wilson@kit.edu)

---

## Table of Contents
  1. [Introduction](#introduction)
  2. [Installation](#install)
      1. [Install Dependencies](#install_dep)
  3. [Ionization Models](#models)
      1. [Simple Model](#simple_model)
      2. [Binomial Model](#binomial_model)
      3. [RK Model](#rk_model)
  4. [Using the Tools](#use)
  5. [Project Status](#status)
  6. [Issue Tracking](#issue_tracking)

---

## Introduction <a name="introduction"></a>
This package provides tools for computing eh-pair ionization probabilities for given energy depositions in a given target material. The ionization probabilities can be calculated using the following methods:
  - Simple
  - [Binomial](https://arxiv.org/src/2005.14067v3/anc/HVeV_Run_2_Data_Release.pdf)
  - [Ramanathan-Kurinsky (RK)](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.102.063026)

The different models are described below.

## Installation <a name="install"></a>
#### Install Dependencies: <a name="install_dep"></a>
-  numpy
-  scipy
-  [dmmodeling_physicsvalues >= 2.2.0](https://gitlab.com/supercdms/public/dmmodeling_physicsvalues.git)

This git repository is also an installable python package. There are various ways to install a python package, but a common way is to first git clone the repository into your local directory. Once that is done, the package can be installed as follows:
```
pip install [path-to-dmmodeling_ehpairquantization-package]/dmmodeling_ehpairquantization
```
or
```
pip install --upgrade [path-to-dmmodeling_ehpairquantization-package]/dmmodeling_ehpairquantization
```
Depending on the user's setup, the installation may or may not be done within a python venv. In some cases, it may be necessary to add the `--user` argument to the pip install command. If the package is installed from a Juptyer notebook, restart the kernal after the package is successfully installed before importing.

This package has an installation dependencies on another custom python package(s) ([dmmodeling_physicsvalues](https://gitlab.com/supercdms/public/dmmodeling_physicsvalues.git)). The installation will attempt to find if the package(s) exists in the correct version(s) and if not, attempt to retrieve the package(s) from Gitlab and install them. This installation process requires the git executable to be available on the system path. ***In some cases, the installation fails to access the dependent package(s) from GitLab and fails to install them. If this happens, the user should manually install the dependent package(s) with the correct version.***

***Package installation works with pip version 21.3.1. Try updating pip if the installation fails.***

---

## Ionization Models <a name="models"></a>

### Simple Model <a name="simple_model"></a>
As the name suggests, this is the simplest ionization model available in this package. For a given energy deposition $E$, (temperature-dependent) band gap energy $E_{gap}$, and average energy to create an eh pair $\epsilon_{eh}$, this method calculates the number of eh pairs as:

$$n_{eh} = \frac{E - E_{gap}}{\epsilon_{eh}}$$.

The pdf returned is a delta function at the rounded-down integer value of the calculated $n_{eh}$.


### Binomial Model <a name="binomial_model"></a>
The binomial model is the method that has been used by previous SuperCDMS HVeV analyses, including [HVeV R1](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.121.051301) and [HVeV R2](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.102.091101).The data release documents associated with these publications provide detailed explanations of this method. 

Again, this method considers the given energy deposition $E$, the band gap energy of the material $E_{gap}$, and the average energy to create an eh pair $\epsilon_{eh}$. If $E$ < $E_{gap}$, no ionization is produced. If $E_{gap} \leq E$ < $\epsilon_{eh}$, 1 eh pair is produced. If $\epsilon_{eh} \leq E$, binomial distributions are constructed such that the average number of eh-pairs produced is equal to $E/ \epsilon_{eh}$. 

The binomial (or negative binomial) distributions are constructed using the given Fano factor $F$, whereby $F$ is equal to the ratio of the variance and the mean of the distribution constructed. A completely uncorrelated (Poisson) process has $F = 1$. However in most radiation detectors $F$ is on the order of 0.1 - 0.2. Using $F$ and the mean number of ionization produced, a binomial or negative binomial distribution is constructed, and the ionization probabilities for the specified eh-pairs are determined.

### RK Model <a name="rk_model"></a>
This model uses the ionization yield results in Si found by [K. Ramanathan and N. Kurinsky](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.102.063026), otherwise known as the "RK" model. This method models the energy redistribution from an initial charge with momentum to the electron-phonon system to determine the subsequent number of eh-pairs produced. This work provides results based on simulations for energy depositions below 50 eV. Above 50 eV, ionization probabilities are determined from an analytic Gaussian function for a given fano factor $F$ and average energy to produce an eh pair $\epsilon_{eh}$. Note that in this model, the values of $F$ and $\epsilon_{eh}$ are not arbitrary. Rather, they are described by parameterized equations and are dependent on the band gap energy $E_{gap}$, and thus also dependent on temperature.

The results from this work for energies below 50 eV are provided in publicly available data files, and are produced for three stated temperatures: 0 K, 100 K, and 300 K. These results are show below.

![Ramanathan_Kurinsky_2020_fig6](Images/Ramanathan_Kurinsky_2020_fig6.png)

Above: Ionization yield results in Si using the RK method. Fig. 6 from K. Ramanathan and N. Kurinsky [https://journals.aps.org/prd/abstract/10.1103/PhysRevD.102.063026](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.102.063026).

In this packages, the results provided at 0 K, 100 K, and 300 K have been compiled into a 2-d interpolated object. These results, however, are not directly interpolated over temperature. Rather, these results scale linearly over $E_{gap}$, which itself is temperature dependent. Therefore the results of this method are loaded by interpolating over the energy and the computed $E_{gap}$ at a given temperature.

***Important Note:*** The results of this work were produced using a specific values of the band gap energy of Si at 0K. However, the 0 K $E_{gap}$ may be different when used by the user (as is the case in the physicsvalues.default_constants object). It has been verified that the results from the RK paper are dependent solely on $E_{gap}$, and not on the parameters that determine the value of $E_{gap}$. For example, if two sets of band gap energy parameters, $E_{gap, A}$ and $E_{gap, B}$ evaluated at two temperatures $T_{A}$ and $T_{B}$ produced the same value of $E_{gap}$, i.e. $E_{gap, A}(T_{A}) = E_{gap, B}(T_{B})$, the ionization yield results will be the same. This means that the ionization yield results are interpolated just based on the temperature and band gap energy parameters provided. If the band gap energy parameters differ from what was used to produce the RK results (as is the default setting in the physicsvalues package), the user should be aware that the limits of the 2-d interpolated object will occur at different temperatures.

---

## Using the Tools <a name="use"></a>
The tools can be used by calling the `calculate_eh_probabilities` function. The `calculate_eh_probabilities` function has the following arguments:

**Parameters**
  - energies : int, float, list, or 1d numpy array
      - energies to evaluate the quantization probabilities in units of eV.
  - eh_grid : int, list, or 1d numpy array
      - integer number of electron-hole pairs to evaluate the quantization probabilities. If eh_grid is a list or 1d numpy array, the quantization probabilities are evaluated for the elements in the list/array. If eh_grid is an int, the quantization probabilities are evaluated for 0 to eh_grid electron-hole pairs.
  - temperature : float or int
      - temperature of the target in units of Kelvin. The temperature input is used to compute the temperature-dependent Egap value. This is calculated in most situations, except when the "simple" model is used and the use_fixed_Eeh argument is True.

**Optional Parameters**
  - target : string, default = "Si"
      - material of the target. The options are "Si".
  - model : string, default = "RK"
      - which model to use to evaluate the quantization probabilities. The options are "simple", "binomial", and "RK".
  - physicsvalues_obj : Dictionary or ImmutableDictionary, default = None
      - an object of the Dictionary class with the constants. Can be obtained from physicsvalues.load_constants() If None, the constants from physicsvalues.default_constants are used.
  - use_fixed_fano : bool, default = False
      - whether to use a fixed value for the Fano factor found in physicsvalues_obj or use the Fano factor calculated using the Ramanathan-Kurinsky method and the parameters found in physicsvalues_obj. Not applicable if the "simple" method is used, and applicable in the "RK" method for energies above 50 eV.
  - use_fixed_Eeh : bool, default = False
      - whether to use a fixed value for the average energy per electron-hole pair (Eeh) found in physicsvalues_obj or use the Fano factor calculated using the Ramanathan-Kurinsky method and the parameters found in physicsvalues_obj. If the "RK" method is used, it is only applicable for energies above 50 eV.
  - gaussian_approx : bool, default = False
      - whether to switch to a offset Gaussian approximation in the "binomial" method of calculating the quantization probabilities. If True, an offset Gaussian distribution is used to calculate the quantization probabilties if the sigma of the distribution is > 5 and the lower edge of the distribution is > 5. This flag is only applicable for the "binomial" method.

**Returns**
  - eh_grid : 1d numpy array
      - the integer number of electron-hole pairs where the quantization probabilities were calculated.
  - eh_pdf : 1d or 2d numpy array
      - the computed quantization probabilities for each energy and electron-hole pair. If the energies argument is an int or float, eh_pdf will be a 1d numpy array with the same length as eh_grid. If the energies argument is a list or numpy array, eh_pdf will be a 2d numpy array with the shape len(energies)xlen(eh_grid).

All of the constants and parameters that this package requires are accessed via the [physicsvalues](https://gitlab.com/supercdms/physicsvalues) package. A simple example of using the tool is shown below.

```python
import ehpairquantization as ehq
import numpy as np

max_neh = 10 # max number of eh pairs. Can also be an array.
temperature = 0.05 # target material temperature

# define grid of energy depositions in units of eV
energies = np.array([5,15])

# compute ionization probabilities for each model
eh_grid, pdf_binom = ehq.calculate_eh_probabilities(energies,max_neh,temperature,model='binomial')
_, pdf_simple = ehq.calculate_eh_probabilities(energies,max_neh,temperature,model='simple')
_, pdf_RK = ehq.calculate_eh_probabilities(energies,max_neh,temperature,model='RK')

```

Executing the code above will produce the results shown in the plots below.

![ehquant_example_5eV](Images/ehquant_example_5eV.png) ![ehquant_example_15eV](Images/ehquant_example_15eV.png)

---

## Project Status <a name="status"></a>
Potential or known updates include:
- adding compatability with Ge target material.
- Adding additional ionization models for other target materials, such as Ge

---

## Issue Tracking <a name="issue_tracking"></a>
For reporting issue with this package or requesting features, please submit a new issue for this project on GitLab. Unfortunately, merge requests are not currently supported for public users of this package. If you are a public user of this package and would like to submit a merge request, please contact the project manager.
