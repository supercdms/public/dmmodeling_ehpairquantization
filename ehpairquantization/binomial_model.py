import numpy as np
from scipy.stats import poisson
from scipy.stats import binom
from scipy.stats import nbinom

def Fanopdf(X,u,F,gaussian_approx):
    '''
    Discrete probabilitiy distribution that has a mean of u and variance of u*F.
    This is done by using binomial or negative binomial distrubitions.
    
    Created by Matt Pyle. Adapted for python by Matt Wilson.
    
    Comments
    --------
    Sometimes it isn't possible to make a discrete probability distribution 
    that matches both the variance and the mean in the case where the variance 
    is abnormally small (i.e for small F and small u).
    In this case, we choose to match the mean over the variance.
    
    The discrete probability distribution with the smallest Fano factor is one 
    which is all zeros except for the interger right below and right above the mean.
    
    Paramaters
    ----------
    X : 1d numpy array
        array of integer number of electron hole pairs to evaluate the 
        discrete probabilities.
    u : float
        mean number of quantization produced.
    F : float
        Fano factor of the distribution.
    gaussian_approx:
        flag to switch to using an offset Gaussian approximation. If True, 
        this is applied if the sigma of the distribution is > 5 and the 
        lower (3-sigma) edge of the distribution is > 5.
    
    '''
 
    if u == 0:
        Y= np.zeros(len(X))
        Y[X==0]=1

        Fdist = 0
    else:    
        # if the expected value is large, use an offset guassian distribution.
        sig = np.sqrt(u*F)
        xledge = u-3*sig
        if sig > 5 and xledge > 5 and gaussian_approx:
            Y=norm(u,sig).pdf(X)

            Fdist = F
        else:
            # since the expected value is small, let's build the distribution from binomial distributions when possible .
            if F < 0.95:
                # mean = np
                # var  = np(1-p)
                # F = var/mean = (1-p) = (1-mean/n)
                # because of quantization, the Fano Factor isn't exactly right:
                # - using ceil above the Fano Factor always pushes towards 1
                # - using floor above pushes the Fano Factor towards 0
                # - when the p required is > 1 ... it means that it's impossible to build the Fano Factor from binomial
                # distributions

                n  = [np.floor(u/(1-F)),np.ceil(u/(1-F))]
                p  = u/n
                Fdist = 1-p
                # find a physically realizable binomial distribution that has a Fano
                # factor which slightly larger than the requested F.
                Yh=binom.pmf(X,n[1],p[1])
                Fh=Fdist[1]
                uh=u

                # find a physically realizable binomial distribution that has a Fano
                # factor which slightly smaller than the requested F.
                if  Fdist[0] >= 0 and p[0]<=1:
                    Yl=binom.pmf(X,n[0],p[0]);

                    Fl=Fdist[0]
                    ul=n
                else:
                    # it's impossible to match the Fano Factor exactly with a binomial distribution.
                    # make the discrete distribution with the absolute 
                    # smallest possible Fano factor
                    Yl= np.zeros(len(X))

                    du = np.mod(u,1)
                    Yl[X== np.floor(u)]= 1-du
                    Yl[X== np.ceil(u)] = du

                    ul= np.floor(u)*(1-du)+ np.ceil(u)*du
                    Fl= (((np.floor(u)**2.0)*(1-du)+ (np.ceil(u)**2.0)*du)-ul**2.0)/ul            

                # linearly combine these 2 distributions to match the wanted Fano Factor
                if Fl == Fh:
                    Y = Yl
                elif F < Fl:
                    Y = Yl
                else :
                    dF = (F-Fl)/(Fh-Fl)
                    Y= Yl*(1-dF)+Yh*dF
            elif F > 1.05:
                # use a negative binomial distribution

                p= 1-1/F
                r= u/(F-1)
                p2 = u/(r+u)

                Y= nbinom.pmf(X,r,1-p)

                Fdist = F
            else:
                # use a poissonian distribution
                Y=poisson.pmf(X,u)

                Fdist=1
    return Y

def calculate_Qprod(energy,Egap,Eeh,fano,gaussian_approx):
    '''
    Function to calculate the electron-hole probabilities for a given 
    energy deposition and fano factor.
    
    Parameters
    ----------
    energy : int or float
        energy deposition.
    Egap : float
        band gap energy.
    Eeh : float
        energy per electron-hole pair
    fano : float
        fano factor
    gaussian_approx: bool
        flag to switch to using a Gaussian approximation for large amounts 
        of expected ionization.
    
    Returns
    -------
    pdfQ : 1d numpy array
        calcuated probability at each eh pair.
    Q_grid : 1d numpy array
        array of electron-hole pairs.
    '''
    
    # calculate the average number of charges
    Q_avg = energy /Eeh

    # calculate the average number of additional charges created
    Qx_avg = (energy / Eeh) -1

    # define the eh pairs that which the probabilties will be calculated for
    # chosen to be +/- 4 sigma
    varQ_continuous= Q_avg*fano

    Qx_lim = [np.ceil(Q_avg) - 4*np.ceil(np.sqrt(varQ_continuous)+1),np.ceil(Q_avg) + 4*np.ceil(np.sqrt(varQ_continuous)+1)]
    
    #let's make certain that the lower Grid edge is >= 0;
    if Qx_lim[0] < 0: Qx_lim[0] = 0

    Qx = np.arange(Qx_lim[0],Qx_lim[1]+1,1)

    # calculate the effective fano factor (fano factor of the cascade)
    fano_eff = fano*Q_avg/Qx_avg

    # do not allow the effective Fano Factor for the cascade to be >1 if
    # fano_eff is < 1
    if fano < 1 and fano_eff > 1: fano_eff = 1
    
    # apply the fano statistics
    pdfQx = Fanopdf(Qx,Qx_avg,fano_eff,gaussian_approx)

    # add the initial e/h pair to the distributions
    Q_Grid = Qx+1
    pdfQ=pdfQx
    
    # apply energy conservation
    Qmax_Econs = np.floor(energy/Egap)
    lgc_Econs = (Q_Grid[:] <= Qmax_Econs)
    pdfQ_cons = sum(pdfQ[lgc_Econs][:])
    
    pdfQ = pdfQ[lgc_Econs]/pdfQ_cons
    Q_Grid = Q_Grid[lgc_Econs]
    
    return pdfQ, Q_Grid

def calculate_Qprod_binomial(energies,eh_grid,Egap,Eeh,fano,gaussian_approx):
    '''
    In this function we calculate the probability distribution for ionization 
    production in a electronic recoil or energy deposition using the binomial
    or negative binomial method.
    
    Created by MCP
    4/4/18:  MCP:  modified so that for energies above Eeh always 1 e/h pair is made.
    
    Adapted for python by MJW.
    
    Paramters
    ---------
    energies : 1d numpy array
        array of energies in which to evaluate the ionization production 
        probabilities.
    eh_grid : 1d numpy array
        the electron-hole pairs that the ionization probabilities will be 
        calculated for.
    Egap : float
        band gap energy of the target material.
    Eeh : float
        energy required to produce 1 electron-hole pair in the target material.
    fano : float
        fano factor of the target material.
    gaussian_approx: bool
        flag to switch to using a Gaussian approximation for large amounts 
        of expected ionization.
    
    Returns
    -------
    eh_pdf : 2d numpy array
        the calculated ionization production probabilties for each energy and 
        electron-hole pair.
        The output will have the dimensions len(energies)xlen(eh_grid).
    '''
    
    # initialize return object
    eh_pdf = np.zeros(shape=(len(energies),len(eh_grid)))
    
    # for energies less than Egap, no quantization is created.
    energy_mask = energies < Egap
    if 0 in eh_grid[:]:
        eh_pdf[energy_mask,np.where(eh_grid[:] == 0)] = 1.
    
    # for energies greater than Egap and less than Eeh, only 1 eh pair created
    energy_mask = (energies >= Egap)&(energies < Eeh)
    if 1 in eh_grid[:]:
        eh_pdf[energy_mask,np.where(eh_grid[:] == 1)] = 1.

    # for energies greater than Eeh, calculate the propabailities using the (negative) binomial distribution
    energy_mask = energies >= Eeh
    Qprod = list(map(lambda a : calculate_Qprod(a,Egap,Eeh,fano,gaussian_approx), energies[energy_mask]))
    
    # map the results to the output grid
    for i, Qi in zip(np.where(energy_mask)[0],Qprod):
        eh_pdf[i,np.where(np.in1d(eh_grid,Qi[1]))[0]] = Qi[0][np.where(np.in1d(Qi[1],eh_grid))[0]]
    
    return eh_pdf