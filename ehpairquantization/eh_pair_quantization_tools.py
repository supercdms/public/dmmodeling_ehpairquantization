import numpy as np
import pickle
import pkg_resources
import logging

from .binomial_model import *

import physicsvalues as pvl

__all__ = [
    "calculate_eh_probabilities",
]

interpolator_str = pkg_resources.resource_filename(__name__, 'RK_ionization/interpolator.pkl')

# on import, load function interpolated from Ramanathan-Kurinsky data
with open(interpolator_str, 'rb') as f:
    peak_interp_list_loaded = pickle.load(f)

def calc_Egap(temperature,constants,target):
    '''
    Calculates the approximate gap energy in Sat any temperature. 
    Using values from the RK paper.

    Parameters
    ---------- 
    temperature : float
        temperature of target in Kelvin.
    constants : Dictionary
        an object of the Dictionary class with the constants.
    target : string
        target material
    
    Returns
    -------
    Egap : float
        Gap energy at the given temperature.
    '''
    
    if target.lower() == 'si':
        Egap0 = constants.Si_params.bandgap_params.Eg10.value # band gap energy at 0 K.
        beta = constants.Si_params.bandgap_params.beta.value
        gamma = constants.Si_params.bandgap_params.gamma.value
    

    Egap = Egap0 - (beta*(temperature**2.0))/(temperature+gamma)
    return Egap

def calculate_simple_eh_pdf(energies,
                  eh_grid,
                  Egap,
                  Eeh):
    '''
    Generates the (delta-function) pdf of eh-pair number using the 
    simplest possible method. Dividing by epsilon and rounding down.

    Parameters
    ---------- 
    energies : numpy 1d array
        recoil energy values in eV.
    eh_grid : numpy 1d array
        array of eh pairs to evaluate the pdf
    Egap : float
        material gap energy in eV.
    Eeh : float
        average energy/eh-pair produced in eV. Material specific.
    
    Returns
    -------
    eh_pdf :  2d numpy array
        the calculated ionization production probabilties for each energy 
        and electron-hole pair.
        The output will have the dimensions len(energies)xlen(eh_grid).
    '''

    # Calculate the quantization based on Eeh
    neh = np.array(1+np.floor((energies-Egap)/Eeh),dtype=int)
    
    # Initialize output
    eh_pdf = np.zeros((len(energies),len(eh_grid)))
    
    # Set the calculated quantization to 100%
    for i, neh_i in enumerate(neh):
        if neh_i in eh_grid:
            eh_pdf[i,np.where(eh_grid == neh_i)[0][0]] = 1.
    
    return eh_pdf

def RK_gaussian(energies,
                peak_n,
                fano,
                Eeh):
    '''
    Generates the pdf of eh-pair number, using Ramanathan-Kurinsky 
    method for recoil energies above 50 eV.

    Parameters
    ---------- 
    energies : 1d numpy array
        energy values in eV.
    peak_n : int
        eh pair of interest.
    fano : float
        Fano factor
    Eeh : float
        average energy/eh-pair in eV.
    
    Returns
    -------
    Probability for that eh number for the given energies.
    '''
    
    # return the probabilities
    return np.exp(-0.5*((peak_n*Eeh-energies)/(Eeh*np.sqrt(peak_n*fano)))**2.0)/np.sqrt(2*np.pi*peak_n*fano)

def calculate_eh_pdf_RK(energies,
              eh_grid,
              Egap,
              Eeh,
              fano):
    '''
    Generates the pdf of eh-pair number, for an array of recoil energies, 
    according to the Ramanathan-Kurinsky ionization method.

    Parameters
    ---------- 
    energies : 1d darray
        array of energy values in eV.
    eh_grid : 1d array of int
        array of eh pairs to evaluate the probabilties.
    Egap : float
        band gap energy of target material in eV. Temperature dependence 
        should already be accounted for.
    Eeh : float
        average energy/eh-pair produced in eV
    fano: float
        Fano factor value
    
    Returns
    -------
    eh_pdf : ndarray
        the calculated ionization production probabilties for each energy
        and electron-hole pair.
        The output will have the dimensions len(energies)xlen(eh_grid).
    '''
    
    # initialize output
    eh_pdf = np.zeros(shape=(len(energies),len(eh_grid)))
    
    # for energies less than Egap, no quantization is created.
    energy_mask = energies < Egap
    if 0 in eh_grid[:]:
        eh_pdf[energy_mask,np.where(eh_grid[:] == 0)] = 1.
        
    # for energies in-between Egap and 1.2 eV, using a step function at Egap for the 1eh case, rather than the peak_interp_list_loaded interpolation.
    # The choice of 1.2 eV value is motivated by the fact that it is the lowest value in peak_interp_list_loaded[0]
    # that is not affected by the imprecise interpolation of a step function.
    energy_mask1 = (energies >= Egap)&(energies < 1.2)
    # for energies between 1.2 and 50 eV, use the interpolated data
    energy_mask2 = (energies >= 1.2)&(energies <= 50)
    # for energies greater than 50 eV, use the Gaussian function
    energy_mask3 = energies > 50
    for eh in eh_grid:
        if eh == 0:
            continue
        elif eh==1:
            eh_pdf[energy_mask1, eh_grid==eh] = 1    
            if len(eh_pdf[energy_mask2]) > 0:
                eh_pdf[energy_mask2, np.where(eh_grid == eh)[0][0]] = peak_interp_list_loaded[eh-1](energies[energy_mask2],Egap)
            eh_pdf[energy_mask3,np.where(eh_grid == eh)[0][0]] = RK_gaussian(energies[energy_mask3],eh,fano,Eeh)
        elif eh <= 20:
            if len(eh_pdf[energy_mask1|energy_mask2]) > 0:
                eh_pdf[energy_mask1|energy_mask2, np.where(eh_grid == eh)[0][0]] = peak_interp_list_loaded[eh-1](energies[energy_mask1|energy_mask2],Egap)
            eh_pdf[energy_mask3,np.where(eh_grid == eh)[0][0]] = RK_gaussian(energies[energy_mask3],eh,fano,Eeh)
        elif eh > 20:
            eh_pdf[energy_mask3,np.where(eh_grid == eh)[0][0]] = RK_gaussian(energies[energy_mask3],eh,fano,Eeh)
        
    # No negative likelihoods
    eh_pdf[eh_pdf<0] *= 0
        
    return eh_pdf

def calculate_eh_probabilities(energies,
                               eh_grid,
                               temperature,
                               target="Si",
                               model="RK",
                               physicsvalues_obj=None,
                               use_fixed_fano=False,
                               use_fixed_Eeh=False,
                               gaussian_approx=False):
    '''
    Main function for calculating the quantization (electron-hole pair) 
    probabilities for given energy depositions and target material. 
    The function has can call upon three different models to calculate 
    the quantization probabilities:
        1. "simple" model: this model finds the average quantization produced 
        by dividing the energy deposition by the average energy to produce an 
        electron-hole pair. The pdf will be a delta function and the 
        rounded-down integer electron-hole pair number.
        2. "binomial" model: this model uses a binomial (or negative binomial) 
        distribution to determine the quantization probabilities given the energy 
        depositions, Fano factor, and average energy to produce an electron-hole pair.
        3. "RK: model: this model uses results of the Ramanathan-Kurinsky 
        ionization model to evaluate the quantization probabilities.
        (https://journals.aps.org/prd/abstract/10.1103/PhysRevD.102.063026)
    
    Parameters
    ----------
    energies : int, float, list, or 1d numpy array
        energies to evaluate the quantization probabilities in units of eV.
    eh_grid : int, list, or 1d numpy array
        integer number of electron-hole pairs to evaluate the quantization 
        probabilities. If eh_grid is a list or 1d numpy array, the quantization 
        probabilities are evaluated for the elements in the list/array. 
        If eh_grid is an int, the quantization probabilities are evaluated 
        for 0 to eh_grid electron-hole pairs.
    temperature : float or int
        temperature of the target in units of Kelvin. The temperature input is 
        used to compute the temperature-dependent Egap value. This is 
        calculated in most situations, except when the "simple" model 
        is used and the use_fixed_Eeh argument is True.
    
    Optional Parameters
    -------------------
    target : string, default = 'Si'
        material of the target. The options are "Si".
    model : string, default = 'RK'
        which model to use to evaluate the quantization probabilities. 
        The options are "simple", "binomial", and "RK".
    physicsvalues_obj : Dictionary or ImmutableDictionary, default = None
        an object of the Dictionary class with the constants. Can be obtained 
        from physicsvalues.load_constants() If None, the constants from 
        physicsvalues.default_constants are used.
    use_fixed_fano : bool, default = False
        whether to use a fixed value for the Fano factor found in 
        physicsvalues_obj or use the Fano factor calculated using the 
        Ramanathan-Kurinsky method and the parameters found in 
        physicsvalues_obj. Not applicable if the "simple" method is used, 
        and applicable in the "RK" method for energies above 50 eV.
    use_fixed_Eeh : bool, default = False
        whether to use a fixed value for the average energy per electron-hole 
        pair (Eeh) found in physicsvalues_obj or use the Fano factor calculated 
        using the Ramanathan-Kurinsky method and the parameters found in 
        physicsvalues_obj. If the "RK" method is used, it is only applicable 
        for energies above 50 eV.
    gaussian_approx : bool, default = False
        whether to switch to a offset Gaussian approximation in the 
        "binomial" method of calculating the quantization probabilities. 
        If True, an offset Gaussian distribution is used to calculate the 
        quantization probabilties if the sigma of the distribution is > 5 and 
        the lower edge of the distribution is > 5. This flag is only 
        applicable for the "binomial" method.
    
    Returns
    -------
    eh_grid : 1d numpy array
        the integer number of electron-hole pairs where the quantization 
        probabilities were calculated.
    eh_pdf : 1d or 2d numpy array
        the computed quantization probabilities for each energy and electron-hole 
        pair. If the energies argument is an int or float, eh_pdf will be a 1d 
        numpy array with the same length as eh_grid. If the energies argument 
        is a list or numpy array, eh_pdf will be a 2d numpy array with the 
        shape len(energies)xlen(eh_grid).
    '''
    
    lgc_1d_output = False
    
    # check the input parameters
    if isinstance(energies,list):
        energies = np.asarray(energies)
    elif isinstance(energies,int) or isinstance(energies,float):
        energies = np.asarray([energies])
        lgc_1d_output = True
    if isinstance(energies,np.ndarray):
        if energies.ndim != 1:
            raise ValueError('ERROR: energies argument must be an array '+\
                             'with 1 dimension.')
        if any(energies<0):
            raise ValueError('ERROR: energies argument must contain only '+\
                             'positive values.')
    else:
        raise TypeError('ERROR: energies argument is not or cannot be '+\
                        'converted to a numpy 1D array.')
    if not isinstance(temperature,int) and not isinstance(temperature,float):
        raise TypeError('ERROR: temperature argument must be either of int or float type.')
    elif temperature < 0:
         raise ValueError('ERROR: temperature value of %s falls below zero. '%temperature+\
                          'Make sure the temperature is given in units of Kelvin!')
    if not isinstance(target,str):
        raise TypeError('ERROR: target argument must be a string type.')
    elif target.lower() != 'si':
        raise NotImplementedError('ERROR: target argument must be one '+\
                                  'of the following options: "Si".')
    if not isinstance(model,str):
        raise TypeError('ERROR: model argument must be a string type.')
    elif model.lower() != 'simple' and model.lower() != 'binomial' and model.lower() != 'rk':
        raise ValueError("ERROR: the model argument '%s' is not accepted. "%model+\
                         "Accepted options include 'simple', 'binomial', or 'RK'!")
    if not isinstance(use_fixed_fano,bool):
        raise TypeError('ERROR: use_fixed_fano argument must be a boolean')
    if not isinstance(use_fixed_Eeh,bool):
        raise TypeError ('ERROR: use_fixed_Eeh argument must be a boolean')
    if not isinstance(gaussian_approx,bool):
        raise TypeError ('ERROR: gaussian_approx argument must be a boolean')
    if isinstance(eh_grid,list):
        eh_grid = np.asarray(eh_grid)
    elif isinstance(eh_grid,int):
        eh_grid = np.arange(0,eh_grid+1,1)
    if isinstance(eh_grid,np.ndarray):
        if eh_grid.ndim != 1:
            raise TypeError('ERROR: eh_grid argument must be an '+\
                            'array with 1 dimension.')
        if not issubclass(eh_grid.dtype.type,np.integer):
            raise TypeError('ERROR: eh_grid argument must be an '+\
                            'array of integers.')
        if any(eh_grid<0):
            raise ValueError('ERROR: eh_grid argument must contain '+\
                             'only postive integers.')
        if len(np.unique(eh_grid)) != len(eh_grid):
            raise ValueError('ERROR: integers in eh_grid argument must be unique.')
    else:
        raise TypeError('ERROR: eh_grid argument is not or cannot be '+\
                        'converted to a numpy 1D array.')
    
    # check for some odd scenarios and warn
    if model.lower() == 'rk':
        if use_fixed_fano or use_fixed_Eeh:
            logging.warning('WARNING: the use_fixed_fano and use_fixed_Eeh arguments '+\
                            'are only applicable in the RK for energies above 50 eV. '+\
                            'By using a use_fixed_fano or use_fixed_Eeh value, there may '+\
                            'be a discrepancy in the results below and above 50 eV.')
    elif model.lower() == 'simple' and use_fixed_fano:
        logging.warning('WARNING: the use_fixed_fano arguments is not applicable in '+\
                        'the simple model. Nothing will happen.')
    if model.lower() != 'binomial' and gaussian_approx:
        logging.warning('WARNING: the gaussian_approx argument is only applicable '+\
                        'for the binomial model.')
    
    # decide which set of constants to use
    if physicsvalues_obj is None:
        # use pre-loaded constants
        physicsvalues_obj = pvl.default_constants
    elif not isinstance(physicsvalues_obj,pvl.Dictionary) and not isinstance(physicsvalues_obj,pvl.ImmutableDictionary):
        raise TypeError('ERROR: physicsvalues_obj was passed but it is not of '+\
                        'type physicsvalues Dictionary class.')
    
    # calculate the bandgap energy at the given temperature.
    Egap = calc_Egap(temperature,physicsvalues_obj,target)
    
    # determine the fano factor and Eeh values.
    if target.lower() == 'si':
        if use_fixed_fano:
            fano = physicsvalues_obj.Si_params.fixed_fano.value
        else:
            fano_c0 = physicsvalues_obj.Si_params.variable_fano_params.c0.value
            fano_c1 = physicsvalues_obj.Si_params.variable_fano_params.c1.value
            fano_A = physicsvalues_obj.Si_params.variable_fano_params.A.value
            fano_c2 = physicsvalues_obj.Si_params.variable_fano_params.c2.value
            fano = fano_c0 + fano_c1*fano_A + fano_c2*Egap
        if use_fixed_Eeh:
            Eeh = physicsvalues_obj.Si_params.fixed_Eeh.value
        else:
            Eeh_c0 = physicsvalues_obj.Si_params.variable_Eeh_params.c0.value
            Eeh_c1 = physicsvalues_obj.Si_params.variable_Eeh_params.c1.value
            Eeh_A = physicsvalues_obj.Si_params.variable_Eeh_params.A.value
            Eeh_c2 = physicsvalues_obj.Si_params.variable_Eeh_params.c2.value
            Eeh = Eeh_c0 + Eeh_c1*Eeh_A + Eeh_c2*Egap
    # TODO: add options for other target materials.
    
    # make sure fano and Eeh values are okay
    if model.lower() == 'rk':
        if Egap < 1.1230219895287958 or Egap > 1.1692:
            raise ValueError('ERROR: The value of Egap is %s. For the RK model, '%Egap+\
                             'the value of Egap must be between 1.123 and '+\
                             '1.1692. It is possible that the input temperature '+\
                             'is causing Egap to fall out of range.')
    if Eeh < 0:
        raise ValueError('ERROR: Eeh value must be positive.')
    if model.lower() != 'simple' and fano < 0:
        raise ValueError('ERROR: fano value must be positive.')

    
    if model.lower() == 'binomial':
        eh_pdf = calculate_Qprod_binomial(energies,eh_grid,Egap,Eeh,fano,gaussian_approx)
    elif model.lower() == 'simple':
        eh_pdf = calculate_simple_eh_pdf(energies,eh_grid,Egap,Eeh)
    elif model.lower() == 'rk':
        eh_pdf = calculate_eh_pdf_RK(energies,eh_grid,Egap,Eeh,fano)
        
    if lgc_1d_output:
        eh_pdf = eh_pdf[0]
    
    return eh_grid, eh_pdf