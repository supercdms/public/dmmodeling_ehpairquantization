# eh-Pair Quantization Modeling Repository - Release Notes
Contacts - Taylor Aralis (taralis@caltech.edu, tbaralis@gmail.com), Matthew Wilson (matthew.wilson@kit.edu)

Record of releases and tags for the ehPairQuantization repository. The tagging procedure follows the [Semantic Versioning](https://semver.org/) method.

---
## v2.0.6
**Date:** September 2, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixed bug in setup file in the get_package_version_hist function. In some cases, git url could not be resolved.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v2.0.5
**Date:** September 1, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated README and installation instructions based on the new name of the public version of this package, as well as public urls to dependent packages. Updated names and urls in setup file.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v2.0.4
**Date:** August 18, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixed a bug in the binomial_model script that had an erroneous condition statement in the case where the input fano factor is < 1 but the effective fano factor is > 1. As with previous iterations of this function, is the input fano factor is < 1 but the effective fano factor is kept at <= 1.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v2.0.3
**Date:** August 15, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated get_package_version_hist function in setup file to avoid installing tagged versions that do not begin with 'v'.

**Changes from Last Release Version:** No change to the functionality from previous version.

---


## v2.0.2
**Date:** August 15, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updates based on ORC review process for this software package.

**Changes from Last Release Version:** Fixed typos in README and added additional information. Added statement in LICENSE. The 'temp' input argument has been renamed to 'temperature'. The 'fixed_fano' input argument has been renamed to 'use_fixed_fano'. The 'fixed_Eeh' input argument has been renamed to 'use_fixed_Eeh'. The input argument 'target' is now an optional argument with the default set to 'Si'. The input argument 'model' is now an optional argument with the default set to 'RK'.

---


## v2.0.1
**Date:** May 12, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated path to physicsvales package in setup file. Updated Issue Tracking section in README.

**Changes from Last Release Version:** No changes to functionality from last release.

---

## v2.0.0
**Date:** Feb 13, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Removed detector response components from the tools. Removed all naming and files related to HVeV-specific analyses. Changed to only one point of entry (i.e. one function to call), `calculate_eh_probabilities`, where you can specific the ionization model to use. No longer seperate functions to call for different ionization models.

Fully incorporated the physicsvalues package - all constants and parameters are loaded from here.

Included _version.py file. Updated setup file to read package version from _version.py file and to have instal dependencies on custom packages. Updated README. Included BSD 3-Clause License.

**Changes from Last Release Version:** 
  - detector response components no longer included in package
  - HVeV-analysis specific language/functions are removed
  - no longer seperate function to call for different ionization models. All models are accessed through the `calculate_eh_probabilities` function.
  - all constants and parameters are now accessed through the physicsvalues package.

---

## v1.0.0
**Date:** Nov 17, 2022

**Author and Email:** Taylor Aralis (taralis@caltech.edu, tbaralis@gmail.com)

**Comments:** Packaging, adding actual info to README, adding release notes.

**Changes from Last Release Version:** N/A
